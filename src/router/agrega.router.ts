import express from "express";
import AgregaController from "../controller/agrega.controller";
import ProfService from "../service/prof.service";
import ClasseService from "../service/classe.service";
import AgregaService from "../service/agrega.service";

const router = express.Router();
const profService: ProfService = new ProfService();
const classeService: ClasseService = new ClasseService()
const agregaService : AgregaService = new AgregaService(profService, classeService)
const controller: AgregaController = new AgregaController(agregaService)
router.get("/prof/:id", controller.getProfById)
router.get("/classe/:id", controller.getClasseById)


export default router;