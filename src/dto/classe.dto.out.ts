export default class ClasseDtoOut {
    id: string;
    nom: string;
    eleves: string[];


    constructor(id: string, nom: string, eleves: string[]) {
        this.id = id;
        this.nom = nom;
        this.eleves = eleves;
    }
}


