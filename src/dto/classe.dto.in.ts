export default class ClasseDtoIn {
    id: string;
    nom: string;
    professeurs?: string[];
    eleves: string[];


    constructor(id: string ="", nom: string ="", professeurs: string[] =[], eleves: string[] = []) {
        this.id = id;
        this.nom = nom;
        this.professeurs = professeurs;
        this.eleves = eleves;
    }
}