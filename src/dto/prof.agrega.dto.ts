import ClasseDtoOut from "./classe.dto.out";
import ProfDtoOut from "./prof.dto.out";

export default class ProfAgregaDto {

    Prof : ProfDtoOut
    classe : ClasseDtoOut[];

    constructor(Prof: ProfDtoOut, classe: ClasseDtoOut[]) {
        this.Prof = Prof;
        this.classe = classe;
    }
}