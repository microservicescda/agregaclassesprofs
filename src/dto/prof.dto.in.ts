export default class ProfDtoIn {
    id: number
    nom: string;
    prenom: string;
    classes ?: string[];
    matieres: string;


    constructor(id: number = 0, nom: string = "", prenom: string = "", classes: string[] = [], matieres: string = "") {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.classes = classes;
        this.matieres = matieres;
    }
}