import ClasseDtoOut from "./classe.dto.out";
import ProfDtoOut from "./prof.dto.out";

export default class ClasseAgregaDto {
    classe : ClasseDtoOut;
    Prof : ProfDtoOut[]


    constructor(classe: ClasseDtoOut, Prof: ProfDtoOut[]) {
        this.classe = classe;
        this.Prof = Prof;
    }
}