export default class ProfDtoOut {
    id: number
    nom: string;
    prenom: string;
    matieres: string;


    constructor(id: number, nom: string, prenom: string, matieres: string) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.matieres = matieres;
    }
}

