import {Request, Response} from "express";
import AgregaService from "../service/agrega.service";

export default class AgregaController {

    private service: AgregaService

    constructor(agregate: AgregaService) {
        this.service = agregate;
    }

    getProfById = async (req: Request, res: Response) => {
        const id = req.params.id
        const data = await this.service.getprofWithClasses(+id)
        res.send(data)
    }

    getClasseById = async (req: Request, res: Response) => {
        const id = req.params.id
        const data  = await this.service.getClasseWithProfs(id)
        res.send(data);
    }
}
