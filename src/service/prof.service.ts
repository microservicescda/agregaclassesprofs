import axios from "axios";
import ProfDtoIn from "../dto/prof.dto.in";
import "dotenv/config";

export default class ProfService {

    getAll = (): Promise<ProfDtoIn[]> => {
        return axios.get(`${process.env.PROFSURL}`).then(items => items.data)
    }

    getById = (id: number): Promise<ProfDtoIn> => {
        return axios.get(`${process.env.PROFSURL}/id/${id}`).then(items => items.data)
    }

    getByIdentite = (nom: string, prenom: string): Promise<ProfDtoIn> => {
        return axios.get(`${process.env.PROFSURL}/name?nom=${nom}&prenom=${prenom}`)
    }

    getByIds = (ids: string[]):Promise<ProfDtoIn[]> => {
        const arr = ids.join();
        return axios.get(`${process.env.PROFSURL}/ids?ids=${arr}`).then(item => item.data)
}
}