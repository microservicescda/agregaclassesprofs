import axios from "axios";
import "dotenv/config";

export default class ClasseService {

    getAll = () => {
        return axios.get(`${process.env.CLASSEURL}`).then(items => items.data)
    }

    getById = (id: string) => {
        return axios.get(`${process.env.CLASSEURL}/${id}`).then(items => items.data)
    }

    getByName = (name: string) => {
        return axios.get(`${process.env.CLASSEURL}/name/${name}`).then(items => items.data)
    }

    getByIds = (ids: string[]) => {
        const arr = ids.join();
        console.log(arr)
        return axios.get(`${process.env.CLASSEURL}/all/ids?ids=${ids}`).then(items => items.data)
    }
}