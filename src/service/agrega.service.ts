import ProfService from "./prof.service";
import ClasseService from "./classe.service";
import ProfAgregaDto from "../dto/prof.agrega.dto";
import ClasseDtoOut from "../dto/classe.dto.out";
import ClasseDtoIn from "../dto/classe.dto.in";
import ProfDtoIn from "../dto/prof.dto.in";
import ClasseAgregaDto from "../dto/classe.agrega.dto";

export default class AgregaService {
    private profService;
    private classService;

    constructor(profService: ProfService, classeService: ClasseService) {
        this.profService = profService;
        this.classService = classeService;
    }

    getprofWithClasses = async (id: number) => {
        const prof: ProfDtoIn = await this.profService.getById(id);
        if(prof.classes) {
            const classe: ClasseDtoIn[] = await this.classService.getByIds(prof.classes);
            const resu: ClasseDtoOut[] = classe.map(item => { delete item.professeurs; return item})
            delete prof.classes;
            const data = new ProfAgregaDto(prof, resu)
            return data
        }
        return null
    }

    getClasseWithProfs = async (id: string) => {
        const classe: ClasseDtoIn = await this.classService.getById(id);
        if(classe.professeurs) {
            const profs: ProfDtoIn[] = await this.profService.getByIds(classe.professeurs);
            console.log(profs)
            profs.map(item => {
                delete item.classes;
                return item
            })
            delete classe.professeurs
            const data = new ClasseAgregaDto(classe, profs)
            return data
        }
    }
}